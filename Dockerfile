# builder
FROM gitlab-registry.eps.surrey.ac.uk/noc/golang:latest

WORKDIR /go/src

USER root

RUN apt-get update
RUN apt-get install -y nano vim curl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin

# Customise PS1
RUN touch /root/.bashrc
RUN echo "[ -z \"\$PS1\" ] && return">> /root/.bashrc
RUN echo "PS1=\"\\[\\033[35m\\][\\u@\\h \\w]:\\[\\033[37m\\]\"">>/root/.bashrc
RUN echo "alias kt='kubectl'">>/root/.bashrc
RUN echo "alias ktp='kubectl get pod -o wide'">>/root/.bashrc
RUN echo "alias kts='kubectl get svc -o wide'">>/root/.bashrc
RUN echo "alias ktd='kubectl get deployment -o wide'">>/root/.bashrc
RUN echo "alias ktn='kubectl get node -o wide'">>/root/.bashrc
RUN echo "alias ktv='kubectl get pv -o wide'">>/root/.bashrc
RUN echo "alias ktvc='kubectl get pvc -o wide'">>/root/.bashrc

EXPOSE 8181
